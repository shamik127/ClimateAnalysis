from __future__ import division, print_function
import pandas as pd
import numpy as np    
    
    
#############################################################################################################################

# Calculates the rise daily in city temperature
# from 2003 to 2017
# Rise in city temperature from 2003-2017 = Avg. city temperature from 2015 to 2017 - Avg. city temperature from 2003 to 2005

# Output : [--------------1d-Array representing the avg temp rise in different cities---------------->]
#############################################################################################################################

def get_temp_rise(data):
    initial_years = np.arange(2003, 2006)
    final_years = np.arange(2015, 2018)
    
    initial_temp = {}
    final_temp = {}
    
    for idx in data.index:
        initial_temp[idx] = []
        final_temp[idx] = []
    
    for year in initial_years:
        dt = data.copy()
        keysList = dt.keys()
        city_str = "Night" + str(year)[2:]
        to_drop = [ky for ky in keysList if (ky[0:len(city_str)] != city_str) and (ky != 'ID')]
        dt.drop(columns=to_drop, inplace=True)
        for idx in dt.index:
            initial_temp[idx].extend(dt.loc[idx].dropna())
            
    mni = np.zeros((len(data.index), 1))
    mnf = np.zeros((len(data.index), 1))
            
    for year in final_years:
        dt = data.copy()
        keysList = dt.keys()
        city_str = "Night" + str(year)[2:]
        to_drop = [ky for ky in keysList if (ky[0:len(city_str)] != city_str) and (ky != 'ID')]
        dt.drop(columns=to_drop, inplace=True)
        for idx in dt.index:
            final_temp[idx].extend(dt.loc[idx].dropna())
            
    for i in range(len(data.index)):
        mni[i] = np.mean(initial_temp[i])
        mnf[i] = np.mean(final_temp[i])
        
    return (mnf - mni).squeeze()



#######################################################################################################

# Taken from table 3 of the Santosh paper (pan india coefficients)
coeffs = [0., 0., 0., 0.0305, 0.0167, 0.000991, -0.00506, 0, 0.0314, 0.0679, 0.111, 0.149, 0., 0., 0.]
year = 2003

# Main data
data = pd.read_csv("Final datasets/UHI17.csv")

new_names =  {'Unnamed: 0': 'ID'}
data.rename(columns=new_names, inplace=True)
data = data.set_index('ID')

data_copy = data.copy()

column_names = data_copy.columns
city_str = "Night" + str(year)[2:]
to_drop = [ky for ky in column_names if (ky[0:len(city_str)] != city_str) and (ky != 'ID')]


# Drop all irrelevant columns
data_copy.drop(columns=to_drop, inplace=True)

# Each key [0-289] represents a city
temperature2003 = {}
temperature2017 = {}


for idx in data_copy.index:
    temperature2003[idx] = (data_copy.loc[idx].dropna() - 273.15).tolist()
    
    
"""
temperature2003 =
                    {
                    0: [------------ array (Daily temperature of the 0-th city)],
                    1: [------------ array (Daily temperature of the 1-th city)],
                    .
                    .
                    .
                    189: [------------ array (Daily temperature of the 189-th city)],
                    }

"""    

    
temp_rise = get_temp_rise(data)

"""
temp_rise =
            {
            0: number (temperature rise for the 0-th city),
            1: number (temperature rise for the 1-th city),
            .
            .
            .
            189: number (temperature rise for the 189-th city),
            }

"""

for ky in temperature2003.keys():
    temperature2017[ky] = temperature2003[ky] + temp_rise[ky]
    
hist2003 = {}
hist2017 = {}
ee = {}


"""
hist2003 = 
            {
            0: [------------ array (temperature histogram of the 0-th city)],
            1: [------------ array (temperature histogram of the 1-th city)],
            .
            .
            .
            189: [------------ array (temperature histogram of the 189-th city)],
            }
            
coeffs = [------------array(energy consumption increase coefficients for individual histogram bars-------)]

"""

"""
ee = 
    {
    0: number (increase in energy demand for 0-th city),
    1: number (increase in energy demand for 1-th city),
    .
    .
    .
    189: number (increase in energy demand for 189-th city),
    }

"""



for ky in temperature2003.keys():
    hist2003[ky], _ = np.histogram(temperature2003[ky], bins=[3*j for j in range(16)])
    hist2017[ky], _ = np.histogram(temperature2017[ky], bins=[3*j for j in range(16)])
    ee[ky] = np.sum(coeffs * (hist2017[ky] - hist2003[ky]))


